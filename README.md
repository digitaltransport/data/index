# List of Places & Projects

[GeoJSON](places.json)

## Cities

In bold: cities for which we have at least one GTFS dataset.

### Africa

- [**Abidjan**](../../../../africa/abidjan): [Abidjan Transport](http://sites.digitaltransport.io/abidjantransport/)
- [**Accra**](../../../../africa/accra): [Accra Mobility](http://sites.digitaltransport.io/accramobility/)
- [**Addis Ababa**](../../../../africa/addis-ababa): WRI Africa
- [**Cairo**](../../../../africa/cairo): [Transport for Cairo](https://transportforcairo.com/)
- [**Douala**](../../../../africa/douala)
- [Djibouti](../../../../africa/djibouti)
- [**Freetown**](../../../../africa/freetown)
- [**Harare**](../../../../africa/harare)
- [**Kampala**](../../../../africa/kampala)
- [**Kumasi**](../../../../africa/kumasi)
- [Lubumbashi](../../../../africa/lubumbashi)
- [Maputo](../../../../africa/maputo): [Chapas Project](https://chapasproject.wordpress.com/)
- [**Nairobi**](../../../../africa/nairobi): [Digital Matatus](http://www.digitalmatatus.com/)

### Latin America

- [Managua](../../../../latin-america/managua): [Mapanica Rutas](https://rutas.mapanica.net/)

## Countries / Regions

- [**Mali**](../../../../africa/mali): [Data Transport](http://data-transport.org/)

## New entry

Do you know a Digital Transport project and want to add it to this list and on [the map](https://digitaltransport4africa.org/projects/)?

You need to request the changes so they can be accepted (or rejected) by [members of this repository](https://gitlab.com/digitaltransport/data/index/-/project_members). This is the [common workflow with git](https://docs.gitlab.com/ee/workflow/forking_workflow.html).

But don't be scared! It's really easy, just follow these 4 simple steps:

1. First, go to the [`README.md` page](README.md) (the file you're reading now!) and click on "Edit", then "Fork".  
This makes a copy (a "fork") of the `index` project (or "repository") to [your projects](https://gitlab.com/dashboard/projects).
2. Now, add your changes to the text (in the [Markdown format](https://docs.gitlab.com/ee/user/markdown.html)).
3. Then, save your changes by clicking on "Commit changes" at the bottom of the page.  
4. Finally, click on "Submit merge request" on the next page. You can explain your request by writing a description before submitting.

That's it! Your request is now [registered](https://gitlab.com/digitaltransport/data/index/-/merge_requests). Members of the project will then review your changes and accept or reject your request.

To be added to the map, this is the same process but with the [`places.geojson`](places.json) file. You can use the tool [geojson.io](http://geojson.io/) to help you.
The `places.geojson` file is then loaded into [a uMap](https://umap.openstreetmap.fr/fr/map/digitaltransportforafrica_220241) displayed on the Digital Transport website.
